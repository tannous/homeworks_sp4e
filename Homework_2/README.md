# Homework 2
Damien Gilliard, IBOIS, EDAR\
Joseph Tannous, IBOIS, EDAR

## Directory Structure: 

```bash
 Homework_2
    ├── CMakeLists.txt
    ├── README.md
    ├── src
    │   └── *.hh/*.cc 
    │   └── plotter.py
    └── .gitignore
```
## Code Structure:
```mermaid
graph TD;
Series.hh-->ComputeArithmetics.hh;
Series.hh-->ComputePi.hh;
Series.hh-->RiemannIntegral.hh;
Series.hh-->DumperSeries.hh;
ComputeArithmetics.hh-->ComputeArithmetics.cc;
ComputePi.hh-->ComputePi.cc;
RiemannIntegral.hh-->RiemannIntegral.cc;
DumperSeries.hh-->WriteSeries.hh;
DumperSeries.hh-->PrintSeries.hh;
WriteSeries.hh-->WriteSeries.cc;
PrintSeries.hh-->PrintSeries.cc;
ComputeArithmetics.hh-->main.cc;
ComputePi.hh-->main.cc;
RiemannIntegral.hh-->main.cc;
WriteSeries.hh-->main.cc;
PrintSeries.hh-->main.cc;
PrintSeries.cc-. linked .->main.cc;
WriteSeries.cc-. linked .->main.cc;
ComputeArithmetics.cc-. linked .->main.cc;
ComputePi.cc-. linked .->main.cc;
RiemannIntegral.cc-. linked .->main.cc;



```
## Dependencies of plotter.py

-matplotlib.pyplot

-csv (this module should be part of your "stock" python, not an external library)

-os (this module should be part of your "stock" python, not an external library)
## Compiling of the Code 

1. Got to the folder `Homework_2`
2. Create and enter a build directory:
   ```bash
   mkdir build
   cd build
   ```
3. Compile the code:
   ```bash
   cmake ..
   make
   ```

### Usage

1. Run the file (inside the `./build` file ):
  ```bash
  ./Homework_2
  ```
2. Follow the instructions on the prompt
    * Which computation will be implemented: ComputeArithmetic, ComputePi or RiemannIntegral;
    * The frequency at which we want to check the result of the computation;
    * The maximum iterations allowed;
    * Whether we want to output to a file or print in the terminal;
    * If we want a file, which separator we want (if ',' we get a .csv, if '|' we get a .psv , and if ' ' we get a .txt file. If another separator is given, it automatically outputs a .txt file with the separator given); 
    * Which precision (significant numbers) we want to keep;
    * If we have asked a Riemann Integral, which of three functions we want to implement ($cos(x)$, $sin(x)$ , or $x^3 $), and then the extremas a and b for the integral;
3. run the plotter.py from the home directory if a .txt file has been outputed with a space ' ' as separator:
```bash
python3 src/plotter.py
```
! Doesn't work properly with RiemannIntegral..


### Results

* Following the instructions on the prompt; You have 2 possible results:

    1 - Print result in terminal

    2 - Write result in file in the build folder  

* we know that the points 3.5, 3.10, 4.5 and 5.3 of the homework have not be fulfilled, by lack of time. We are learning a lot and we are immensely thankful for that, but we struggle sometimes on some points and we prefer to skip them to do everything we can.
