#include "PrintSeries.hh"
#include <iostream>
#include <math.h>
PrintSeries::PrintSeries(int maxiter, int freq, Series& series): DumperSeries(series), freq(freq), maxiter(maxiter) {}

void PrintSeries::setPrecision(unsigned int precision){
    this-> precision = precision;
    std::cout<<"precision = "<<precision<<std::endl;
}

void PrintSeries::dump(std::ostream & os ){
    for( int i = 1;i <= maxiter/freq; i++){
        double inter_res = series.compute(i*freq);
        //double conv =  100* inter_res/M_PI;
        os.precision(precision);
        os<<i*freq<<" : "<<inter_res<</*" Convergence = "<<conv<<"%"<<*/std::endl;

    }
    //os<<"is that really what question 3.5 asks for ?"<<std::endl;
}