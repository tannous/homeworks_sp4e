#include <iostream>
#include <istream>
#include <exception>
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "RiemannIntegral.hh"
int main(int argc, char** argv){

    std::cout<<"What Scenario Would You like to implement? CA = Arithmetic Series , PI = Pi Computation , RI = Riemann Integral "<<std::endl;
    std::string scenario;
    std::cin>>scenario;

    std::cout<<"What is the frequency "<<std::endl;
    int frequency;
    std::cin>>frequency;

    std::cout<<"What is the maximum iteration "<<std::endl;
    int maximum_iteration;
    std::cin>>maximum_iteration;

    std::cout<<"Would you like to print result in terminal or put in file ? Answer t or f."<<std::endl;
    char choice;
    std::cin>>choice;
    char separator;
    if(choice=='f'){
        std::cout<< "you want to create a file."<<std::endl;
        std::cout<<"Would you like to store the file as .txt (enter ' '), .csv (enter ','), or psv (enter '|') ?"<<std::endl;
        // Clear the input buffer, because the last newline is still in it and messes with the cin.get()
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        separator=std::cin.get();
        //std::getline(std::cin, separator);
        //std::cin.get();
        std::cout<<"separator = "<<separator<<std::endl;        
        

    }
    else if(choice == 't'){
        std::cout<<"you want to output to terminal ";
    }
    else if(choice != 't' || 'f'){
        throw std::exception();
        //std::cout<<"didn't enter the right character... try again with 't' or 'f' "<<std::endl;

    }     
    
   
    




    std::cout<<"Which precision would you like ? Give integer"<<std::endl;
    int precision;
    std::cin>>precision;


    Series* series;


    //int frequency = std::atoi(argv[2]);
    //int maximum_iteration = std::atoi(argv[3]);
    //char separator = *argv[4];

    if (std::string(scenario) == "CA" ){

        series = new ComputeArithmetic();

    }
    else if (std::string(scenario) == "PI" ){

        series = new ComputePi();

    }
    else if (std::string(scenario) == "RI" ){

        std::cout<<"What Integral Would You like to implement? c = cos(x), s = sin(x), q = x^3 "<<std::endl;
        char f;
        std::cin>>f;

        std::cout<<"Enter Attribute a"<<std::endl;
        double a;
        std::cin>>a;

        std::cout<<"Enter Attribute b"<<std::endl;
        double b;
        std::cin>>b;
  
        series = new RiemannIntegral(a,b,f);
    

    }  
    else{
        std::cout<<"The computation asked doesn't exist, try CA, RI or PI"<<std::endl;
    }

    if(choice=='t'){
        PrintSeries printer(maximum_iteration,frequency, *series);
            printer.setPrecision(precision);
            printer.dump();
        }
        else if (choice=='f'){
            WriteSeries writer(maximum_iteration,frequency,*series);
            writer.setSeparator(separator);
            writer.dump();
        }
        else{
            std::cout<<"Sorry the parameter given is not t (for terminal) or f (for file)"<<std::endl;
        }



    
    //Because the series pointer created at the beginning of this main.cc is located in the Heap, it must be explicitly deleted
    delete series;
    return 0;    

}
