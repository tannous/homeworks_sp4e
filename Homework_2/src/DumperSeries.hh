#ifndef DUMPERSERIES_HH
#define DUMPERSERIES_HH
#include "Series.hh"
#include <iostream>
class DumperSeries{
    public:
        DumperSeries(Series& series):series(series){}

        //The following line is causing troubles for reasons we must understand !
        //virtual ~DumperSeries();


        virtual void dump(std::ostream & os) = 0;

        virtual void setPrecision(unsigned int precision) = 0;


    
    protected:
        Series& series;    
        /*
        This needs to be figured out...

        
        inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) {
            _this.dump(stream);
            return stream;
            }    
        
        
        */

};
#endif