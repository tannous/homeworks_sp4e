#ifndef SERIES_HH
#define SERIES_HH

class Series{
    public: 
        virtual double compute(unsigned int N) = 0;
        virtual double getAnalyticalPrediction(unsigned int N)=0;
    protected:
        unsigned int current_index;
        double current_value;
};
#endif