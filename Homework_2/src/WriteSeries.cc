#include "WriteSeries.hh"
#include <iostream>
#include <fstream>
#include <math.h>

WriteSeries::WriteSeries(int maxiter, int freq, Series& series): DumperSeries(series), freq(freq), maxiter(maxiter) {}


void WriteSeries::setSeparator(char separator){
    this->separator = separator;
}

void WriteSeries::setPrecision(unsigned int precision){
    this->precision = precision;
}


void WriteSeries::dump(std::ostream & os){
    std::ofstream file_out;

    if(separator == ','){
        file_out.open("output_Homework.csv");
    }   
    else if(separator == '|'){
        file_out.open("output_Homework.psv");
    }
    else{
        file_out.open("output_Homework.txt");
    }    
        if(file_out.is_open()){

            file_out<< series.getAnalyticalPrediction(maxiter)<<separator<<'0'<<"\n";

            for( int i = 1;i <= maxiter/freq; i++){
                double inter_res = series.compute(i*freq);
                file_out << i*freq<<separator<<inter_res<<"\n";
            }  
        }
        else std::cout<<"file not opened!!"<<std::endl;        
    

    
    file_out.close();
}
