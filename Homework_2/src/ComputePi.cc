#include "ComputePi.hh"

double ComputePi::compute(unsigned int N){
        double res = 0;
        for(double i=N; i>=1; i--){
            auto series = [i, &res](){
                res += (1/(i*i));
                return res;
            };
        series();
        }
    double pi = sqrt(6*res);
    return pi;

}
double ComputePi::getAnalyticalPrediction(unsigned int N){
    return M_PI;
}