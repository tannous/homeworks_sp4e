#include "ComputeArithmetic.hh"
double ComputeArithmetic::compute(unsigned int N)
    {
            double res=0;
            for (int i=1; i<=N; i++){
                auto series = [i, &res](){
                    res +=i;
                    return res;
                };
                series();
                
            };
            return res;
        }
double ComputeArithmetic::getAnalyticalPrediction(unsigned int N){
    int result = (N*(N+1))/2;
    return result ;
}