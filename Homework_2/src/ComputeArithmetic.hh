#include "Series.hh"
#ifndef COMPUTE_ARITHMETICS
#define COMPUTE_ARITHMETICS
class ComputeArithmetic : public Series{
    public:

        double compute (unsigned int N);
        double getAnalyticalPrediction(unsigned int N) override;
};
#endif