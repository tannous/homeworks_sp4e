#ifndef PRINTSERIES_HH
#define PRINTSERIES_HH
#include <iostream>
#include "DumperSeries.hh"
class PrintSeries : public DumperSeries{
    private:
        int maxiter;
        int freq;
        int precision;

    public:
        PrintSeries(int maxiter,int freq, Series& series);
        
        void setPrecision(unsigned int precision)override;
        void dump(std::ostream & os = std::cout)override;
};
#endif