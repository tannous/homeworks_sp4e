#include "RiemannIntegral.hh"
#include <iostream>
#include <math.h>

RiemannIntegral::RiemannIntegral(double a, double b, char f): a(a), b(b), f(f){};/**/

double RiemannIntegral::compute(unsigned int N){
    double integral = 0; 

    //This is not very elegant, we should have defined then passed the function as reference to the lambda
        if(f== 'c'){
            double step = (b-a)/N;
            for(int i = 0; i<N;i++){
                auto riemann_lambda = [i,step, &integral](){
                    double temp = cos((i*step)+step/2);
                    integral = integral+(temp*step);
                    return integral;
                };
            riemann_lambda();
             
            }
        }
        else if(f == 's'){
            double step = (b-a)/N;
            for(int i = 0; i<N;i++){
                auto riemann_lambda = [i,step, &integral](){
                    double temp = sin((i*step)+step/2);
                    integral = integral+(temp*step);
                    return integral;
                };
            riemann_lambda();
             
            }
        }
        else if(f == 'q'){
            double step = (b-a)/N;
            for(int i = 0; i<N;i++){
                auto riemann_lambda = [i,step, &integral](){
                    double temp = (i*step+(step/2))*(i*step+(step/2))*(i*step+(step/2));
                    integral = integral+(temp*step);
                    return integral;
                };
            riemann_lambda();
             
            }
        }
        else{
            std::cout<<"the parameter is not recognized, give s (sin), c (cosin), q (x³)"<<std::endl;
        }
        return integral;
    std::cout<<integral<<std::endl; 
    };




double RiemannIntegral::getAnalyticalPrediction(unsigned int N){
    return 0.0;
}

