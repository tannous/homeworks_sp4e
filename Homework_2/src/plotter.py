import matplotlib.pyplot as plt
import csv
import os

cur_path = os.path.dirname(__file__)
new_path = os.path.relpath('build/output_Homework.txt')
x=[]
max=[]
y=[]
with open(new_path,'r') as source_file:
    data = csv.reader(source_file, delimiter=' ')

    for row in data:
        x.append(float(row[0]))
        y.append(float(row[1]))
        
for i in range(len(x)-1):
    max.append(x[0])

fig = plt.figure()
progress = fig.add_subplot(1,1,1)
progress.plot(x[1:],y[1:])
progress.plot(x[1:], max)
plt.show()