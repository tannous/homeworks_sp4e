#include <iostream>
#include <math.h>
#include "Series.hh"
#ifndef COMPUTE_PI
#define COMPUTE_PI
class ComputePi : public Series {
    public:
        double compute (unsigned int N);
        double getAnalyticalPrediction(unsigned int N) override;
};
#endif
