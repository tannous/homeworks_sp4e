#ifndef WRITESERIES_HH
#define WRITESERIES_HH
#include "DumperSeries.hh"
#include <iostream>
class WriteSeries : public DumperSeries{
    private:
    int maxiter;
    int freq;
    char separator;
    int precision;

    public:
        WriteSeries(int maxiter,int freq, Series& series);
        void setSeparator(char separator);
        void dump(std::ostream & os = std::cout)override;// as I understand, using override signals to the compiler that it must check the mother class
        void setPrecision(unsigned int precision)override;// for the same function, so we do not create another, slightly different, function in parallel by mistake 
};
#endif
