#include "Series.hh"
#include <iostream>
#ifndef RIEMANN_HH
#define RIEMANN_HH

class RiemannIntegral : public Series{
    public:
        RiemannIntegral(double a, double b, char f);

        double compute(unsigned int N)override;
        double getAnalyticalPrediction(unsigned int N)override;
      
    private:
        double a;
        double b;
        char f;    
};
#endif