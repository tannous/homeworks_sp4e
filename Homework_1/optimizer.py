import scipy.optimize
import scipy.sparse.linalg
import numpy as np
import pandas
import matplotlib.pyplot as plt
import argparse


############################################################ THIS PART COULD BE IN ANOTHER FILE ################
#Create functor that takes the parameters A and b 

class Problem_initializer (object):

    def __init__(self):
        #gets A and b from csv/txt file
        temp=pandas.read_csv("b.csv", header=None)
        self.vector=temp.values.reshape(2,1)
        temp=pandas.read_csv("A.csv", header=None)
        self.Matrice=temp.values.reshape(2,2)

    def __call__(self,method,quadf,x0):
        #if "LGMRES", apply LGMRES, idem for BFGS
        if method=='LGMRES':
            return self.__LGMRESminimize(self.Matrice,self.vector,x0,self.callback_method)
        elif method=='BFGS':
            return self.__BFGSminimize(quadf, x0,callback=self.callback_method)
        
    def callback_method(self,x):
        intermediary_pts[0].append(x[0])
        intermediary_pts[1].append(x[1])

    def __BFGSminimize(self,quadf,x0,callback):
        return(scipy.optimize.minimize(quadf, x0, method='BFGS',callback=callback))

    def __LGMRESminimize(self,A,b,x0,callback):
        return(scipy.sparse.linalg.lgmres(A,b,x0,callback=callback))

#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


################## USING THE FUNCTOR CREATED ABOVE #################################

parser = argparse.ArgumentParser(
                    prog='optimizer',
                    description='This program implements two methods for minimizing a quadratic function of the type x^t*A*x-b*x, with A a 2x2 matrix and b a 1x2 vector',
                    epilog='This code is far from optimal, but it works. Sorry in any case')

parser.add_argument('-method', '--m',type=str,required=True,help='method used. Either LGMRES or BFGS')
parser.add_argument('-pt', '--pt',type=float,nargs='+',required=True,help='array with the coordinates of initial point')
args = parser.parse_args()

intermediary_pts=[[],[]]
intermediary_z=[]

class Calling_minimizer(object):
    def __init__(self):
        self.minimize=Problem_initializer()

    def implement(self,method, quadf,x0):
        return self.minimize(method, quadf,x0)

Call=Calling_minimizer()

def quadf(param):
    A=Call.minimize.Matrice
    b=Call.minimize.vector
    x=np.array(param)
    return(0.5*np.matmul(np.matmul(x,A),np.matrix.transpose(x))-np.matmul(x,b))
res=Call.implement(args.m,quadf,np.array(args.pt).reshape(2,1))


########################Plotting the surface####################################

x_1=np.arange(-10,10,0.1)
x_2=np.arange(-10,10,0.1)
x_1,x_2=np.meshgrid(x_1,x_2)

#this is cursed, sorry for that: 
def f(x_1,x_2):
    return(0.5*((Call.minimize.Matrice[0][0]*x_1**2)+(Call.minimize.Matrice[1][0]*x_1*x_2)+(Call.minimize.Matrice[0][1]*x_1*x_2)+(Call.minimize.Matrice[1][1]*x_2**2))-x_1*Call.minimize.vector[0]-x_2*Call.minimize.vector[1])

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

#plotting the surface
ax.plot_surface(x_1, x_2, f(x_1,x_2),edgecolor='royalblue', lw=0.5, rstride=8, cstride=8,alpha=0.3)

for i in range(len(intermediary_pts[0])):
    temp=f(intermediary_pts[0][i],intermediary_pts[1][i])
    intermediary_z.append(temp[0])
pt=[]

if type(res)==tuple:
    pt=res[0]
if type(res)==scipy.optimize._optimize.OptimizeResult:
    pt=res.x
print(pt,"is the local minima found")

final_point = ax.scatter(pt[0],pt[1],f(pt[0],pt[1]),c=255, marker="o")
intermediary_points=ax.plot(intermediary_pts[0],intermediary_pts[1],intermediary_z,marker="o",c="red",markersize=10)
plt.show()
