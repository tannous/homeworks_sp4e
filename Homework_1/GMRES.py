import numpy as np


#implement the GMRES method to find the solution of the equation
#independent of A and b size...

M=np.array([[1.0,1.0],[1.0,2.0]])
v=np.array([0.0,10.0])
x=np.array([1.0,1.0])

#1 calculate q_n with the arnoldi method: VARIANT OF CODE FROM WIKIPEDIA :https://en.wikipedia.org/wiki/Arnoldi_iteration

def Arnoldi(A,Q,k):
    h=np.zeros(k+2)
    q=np.einsum('ij,j->i',A,Q[:,k])
    for i in range(k):
        h[i]=np.einsum('i,i->',q,Q[:,i])
    h[k+1]=np.linalg.norm(q)
    return q,h
def get_rotation(u,v):
    hypothenuse=np.sqrt(np.einsum('k,k->', u, u)+np.einsum('k,k->', v, v))
    cos=u/hypothenuse
    sin=v/hypothenuse
    return cos, sin

def apply_rotation(h,cos,sin,k):
    for i in range (k-1):
        temp = cos[i]*h[i]+sin[i]*h[i+1]
        h[i+1]= -sin[i]*h[i]+cos[i]*h[i+1]
        h[i]=temp
    cos_k,sin_k=get_rotation(h[k],h[k+1])
    h[k]=cos_k*h[k]+sin_k*h[k+1]
    h[k+1]=0
    print(h)
    return h,cos_k,sin_k

def GMRES(A,b,x,max_it,threshold):
    n=np.max(np.size(A))
    r=b-np.einsum('ij,j->i',A,x)
    norm_b = np.linalg.norm(b)
    norm_r = np.linalg.norm(r)
    normalized_b = b / np.linalg.norm(b)
    err=norm_r/norm_b
    print("err= ",err)
    Q = np.zeros((A.shape[0], n + 1))

    #2 find the y_n which minimizes the norm of r_n
    sin=np.zeros((max_it,1))
    cos=np.zeros((max_it,1))
    e1=np.zeros((max_it+1,1))
    e1[0] = 1
    Q[:, 0] = r/norm_r
    beta=norm_r*e1
    
    for k in range (max_it):
        Q[:,k+1],H[1:k+1,k]=Arnoldi(A,Q,n)

#3 compute x_n=x_0+Q_n*y_n
GMRES(M,v,x,10,1)
#4 repeate if the residual is not yet small enough

