	
# Homework 1
Damien Gilliard, IBOIS, EDAR\
Joseph Tannous, IBOIS, EDAR

Sadly, only the optimizer.py runs correctly.
The GMRES.py is far from usable. We are sorry for that.
## Dependencies 

- numpy
- argparse
- pandas
- scipy.optimize
- scipy.sparse.linalg
- matplotlib.pyplot

## Usage of the Code 
 The matrix A and vector b are stored in two .csv files called "A.csv" and "b.csv"
### Command line arguments

* -h: help
* -method: The method to use. either LGMRES or BFGS
* -pt: The initial point

### LGMRES optimizer

python3 optimizer.py -method 'LGMRES' -pt 3 3

### BFGS optimizer

python3 optimizer.py -method 'BFGS' -pt 5 5