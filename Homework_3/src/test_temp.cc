#include "compute_temperature.hh"
#include "material_point.hh"
#include "system.hh"
#include <gtest/gtest.h>
#include <cmath>
#include <memory>

// Fixture class for ComputeTemperature tests
class TestTemp : public ::testing::Test {
protected:
    System system;
    std::shared_ptr<ComputeTemperature> temperature;

    void SetUp() override {
        temperature = std::make_shared<ComputeTemperature>();
        temperature->setDeltaT(1e-8); // Small deltaT for equilibrium
        temperature->setMassDensity(1.0);
        temperature->setSpecificHeatCapacity(1.0);
        temperature->setHeatConductivity(1.0);
        temperature->setNullBorder(false); // No heat flux implies null border conditions
    }
};

// Test 2: Initial homogeneous temperature with no heat flux
TEST_F(TestTemp, InitialHomogeneousTemperatureNoHeatFlux) {
    int mat_size = 128;
    double init_temp = 0.0; // Homogeneous initial temperature

    // Create particles with initial temperature and no heat source
    for (int i = 0; i < mat_size; ++i) {
        for (int j = 0; j < mat_size; ++j) {
            MaterialPoint p;
            p.getTemperature() = init_temp; // Homogeneous temperature
            p.getHeatSource() = 0.0; // No heat source
            p.getPosition()[0] = -1.0 + (i * 2.0 / mat_size);
            p.getPosition()[1] = -1.0 + (j * 2.0 / mat_size);
            system.addParticle(std::make_shared<MaterialPoint>(p));
        }
    }

    temperature->initRangeFromParticles(system);
    temperature->compute(system);

    // Check if the final temperature matches the initial homogeneous temperature
    for (auto &par : system) {
        ASSERT_NEAR(static_cast<MaterialPoint &>(par).getTemperature(), init_temp, 1e-15);
    }
}
// Test 3: Equilibrium with sinusoidal volumetric heat source
TEST_F(TestTemp, EquilibriumWithSinusoidalHeatSource) {
    int mat_size = 128;
    double L = 2.0; // Domain length from -1 to 1

    // Create particles with sinusoidal heat source and initial temperature
    for (int i = 0; i < mat_size; ++i) {
        for (int j = 0; j < mat_size; ++j) {
            double x = -1.0 + (i * L / mat_size);
            MaterialPoint p;
            p.getTemperature() = sin(2.0 * M_PI * x / L); // Initial temperature field
            p.getHeatSource() = std::pow(2.0 * M_PI / L, 2) * sin(2.0 * M_PI * x / L); // Volumetric heat source
            p.getPosition()[0] = x;
            p.getPosition()[1] = -1.0 + (j * L / mat_size);
            system.addParticle(std::make_shared<MaterialPoint>(p));
        }
    }

    temperature->initRangeFromParticles(system);
    temperature->compute(system);

    // Check if the final temperature matches the expected equilibrium temperature
    for (auto &par : system) {
        double x = par.getPosition()[0];
        double expected_temp = sin(2.0 * M_PI * x / L);
        ASSERT_NEAR(static_cast<MaterialPoint &>(par).getTemperature(), expected_temp, 1e-6);
    }
}

// Test 4: Equilibrium with piecewise volumetric heat source
TEST_F(TestTemp, EquilibriumWithPiecewiseHeatSource) {
    int mat_size = 128;
    double L = 2.0; // Domain length from -1 to 1

    // Create particles with piecewise heat source and initial temperature
    for (int i = 0; i < mat_size; ++i) {
        for (int j = 0; j < mat_size; ++j) {
            double x = -1.0 + (i * L / mat_size);
            double temp;
            double heatSrc;

            // Define the temperature and heat source according to piecewise functions
            if (x <= -0.5) {
                temp = -x - 1;
                heatSrc = -1;
            } else if (x > 0.5) {
                temp = -x + 1;
                heatSrc = 1;
            } else {
                temp = x;
                heatSrc = 0;
            }

            MaterialPoint p;
            p.getTemperature() = temp;
            p.getHeatSource() = heatSrc;
            p.getPosition()[0] = x;
            p.getPosition()[1] = -1.0 + (j * L / mat_size);
            system.addParticle(std::make_shared<MaterialPoint>(p));
        }
    }

    temperature->initRangeFromParticles(system);
    temperature->compute(system);

    // Check if the final temperature matches the expected equilibrium temperature
    for (auto &par : system) {
        double x = par.getPosition()[0];
        double expected_temp;
        if (x <= -0.5) {
            expected_temp = -x - 1;
        } else if (x > 0.5) {
            expected_temp = -x + 1;
        } else {
            expected_temp = x;
        }
        ASSERT_NEAR(static_cast<MaterialPoint &>(par).getTemperature(), expected_temp, 1e-6);
    }
}

// Main function to run all tests
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}