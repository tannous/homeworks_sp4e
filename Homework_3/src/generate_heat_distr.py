import argparse
import numpy as np
import matplotlib.pyplot as plt
import csv

def parse_arguments():
    """
    Parse command line arguments for the script.
    Returns parsed arguments including grid size, domain limits, heat radius, and output options.
    """
    parser = argparse.ArgumentParser(description='Generate and plot a heat distribution within a grid.')
    parser.add_argument('--xlim', nargs=2, type=float, default=[-1.0, 1.0], help='X-axis limits as two floats.')
    parser.add_argument('--ylim', nargs=2, type=float, default=[-1.0, 1.0], help='Y-axis limits as two floats.')
    parser.add_argument('--pn_x', type=int, default=8, help='Number of points in the x direction.')
    parser.add_argument('--pn_y', type=int, default=8, help='Number of points in the y direction.')
    parser.add_argument('--radius', type=float, default=0.5, help='Radius of the heat distribution.')
    parser.add_argument('--filename', type=str, default='heat_distribution.csv', help='Filename for the output CSV.')
    parser.add_argument('--plot', action='store_true', help='Flag to plot the heat distribution.')
    return parser.parse_args()

def calculate_heat_distribution(xlim, ylim, pn_x, pn_y, radius):
    """
    Calculates the heat distribution over a grid.
    Args:
        xlim, ylim: Domain limits for x and y axes.
        pn_x, pn_y: Number of points in x and y directions.
        radius: Radius for the heat source circle.

    Returns a numpy array representing grid data including positions, temperature, and heat rate.
    """
    # Grid size calculation
    grid_size = pn_x * pn_y

    # Generate grid points
    x_vals = np.linspace(xlim[0], xlim[1], pn_x)
    y_vals = np.linspace(ylim[0], ylim[1], pn_y)

    # Initialize arrays for grid data
    positions = np.zeros((grid_size, 3))
    temperature = np.zeros(grid_size)
    heat_rate = np.zeros(grid_size)

    # Fill grid data
    ind = 0
    for x in x_vals:
        for y in y_vals:
            positions[ind] = [x, y, 0]
            temperature[ind] = apply_boundary_constraint(x, y, xlim, ylim)
            heat_rate[ind] = apply_heat_function(x, y, radius)
            ind += 1

    # Combine all data into one array
    return np.hstack((positions, np.zeros((grid_size, 3)), np.zeros((grid_size, 3)), np.ones((grid_size, 1))*1e9, temperature[:, None], heat_rate[:, None]))

def apply_heat_function(x, y, radius):
    """
    Determines if a point (x, y) is within the heat source circle.
    Args:
        x, y: Point coordinates.
        radius: Radius of the heat source circle.

    Returns 1 if the point is within the circle, 0 otherwise.
    """
    return 1 if x**2 + y**2 < radius**2 else 0

def apply_boundary_constraint(x, y, xlim, ylim):
    """
    Apply boundary constraint to the temperature field.
    Args:
        x, y: Point coordinates.
        xlim, ylim: Domain limits for x and y axes.

    Returns 0 if the point is on the boundary, otherwise uses the heat function.
    """
    # Check if the point is on the boundary
    if x == xlim[0] or x == xlim[1] or y == ylim[0] or y == ylim[1]:
        return 0
    else:
        return apply_heat_function(x, y, 0.5)

def save_data_to_csv(filename, data, decimal_places=4):
    """
    Save grid data to a CSV file.
    Args:
        filename: Name of the file to save data.
        data: Grid data to be saved.
        decimal_places: Precision for floating point numbers.
    """
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ')
        for row in data:
            formatted_row = [f'{value:.{decimal_places}f}' if isinstance(value, float) else value for value in row]
            writer.writerow(formatted_row)
    print(f"Heat distribution data saved to {filename}")

def plot_heat_distribution(pn_x, pn_y, temperature):
    """
    Plot the heat distribution grid.
    Args:
        pn_x, pn_y: Number of points in x and y directions.
        temperature: Array of temperature values.
    """
    plt.figure()
    plt.pcolormesh(temperature.reshape((pn_x, pn_y)), cmap='hot')
    plt.colorbar(label='Temperature')
    plt.xlabel('X Position')
    plt.ylabel('Y Position')
    plt.title('Heat Distribution Grid')
    plt.show()

def main():
    # Parse arguments and calculate data
    args = parse_arguments()
    data = calculate_heat_distribution(args.xlim, args.ylim, args.pn_x, args.pn_y, args.radius)

    # Save and plot data
    save_data_to_csv(args.filename, data)
    if args.plot:
        temperature = data[:, -2]  # Extracting the temperature column
        plot_heat_distribution(args.pn_x, args.pn_y, temperature)

if __name__ == '__main__':
    main()
