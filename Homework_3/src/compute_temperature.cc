#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>
#include <complex>
#include <vector>
#include <iostream>
#include <iomanip>
 

/* -------------------------------------------------------------------------- */

// Constructor for ComputeTemperature class with default values
ComputeTemperature::ComputeTemperature(){

    this->deltaT = 0.001; // Time step for the simulation
    this->rho = 1.0; // Mass density of the material
    this->heatCapacity = 1.0; // Specific heat capacity of the material
    this->heatConductivity = 1.0; // Thermal conductivity of the material
    this->null_border = false; // Flag for boundary condition (false by default)
    this->heat_src_fft_done = false; // Flag to check if heat source FFT is computed
}
 
/* -------------------------------------------------------------------------- */

// Setter methods for different properties
// Set the time step
void ComputeTemperature::setDeltaT(Real deltaT) { this->deltaT = deltaT; }

// Set the mass density
void ComputeTemperature::setMassDensity(Real rho) { this->rho = rho; }

// Set the specific heat capacity
void ComputeTemperature::setSpecificHeatCapacity(Real heatCapacity) { this->heatCapacity = heatCapacity; }

// Set the thermal conductivity
void ComputeTemperature::setHeatConductivity(Real heatConductivity) { this->heatConductivity = heatConductivity; }

// Set the boundary condition flag
void ComputeTemperature::setNullBorder(bool null_border) { this->null_border = null_border; }

/* -------------------------------------------------------------------------- */

// Initialize the spatial range of the system from particle positions
void ComputeTemperature::initRangeFromParticles(System &system){
    // Populate the range vector with x-coordinates of particles
    for (auto &par : system){
        this->range.push_back(par.getPosition()[0]);
    }

    // Sort and remove duplicate elements in range
    std::sort(this->range.begin(), this->range.end());
    auto end = std::unique(this->range.begin(), this->range.end());
    this->range.erase(end, this->range.end());

    // Calculate the spatial step size
    this->dx = range[1] - range[0];
}

/* -------------------------------------------------------------------------- */

// Main method to compute the temperature field
void ComputeTemperature::compute(System &system){
    int mat_size = sqrt(system.getNbParticles()); // Calculate matrix size from the number of particles

    // Initialize matrices for current temperature and heat rates
    Matrix<std::complex<double>> current_temp(mat_size);
    Matrix<std::complex<double>> heat_rates(mat_size);

    // Initialize the heat source matrix only once
    if (!heat_src_fft_done)
    {
        Matrix<std::complex<double>> heat_src(mat_size);
        for (auto &par : system){
            // Calculate indices in the heat source matrix
            int i = std::round((par.getPosition()[0] - this->range[0]) / dx);
            int j = std::round((par.getPosition()[1] - this->range[0]) / dx);
 
            // Assign the heat source value
            heat_src(i, j) = static_cast<MaterialPoint &>(par).getHeatSource();

        }

       // Fourier Transform of the heat source matrix
        this->heat_src_fft = FFT::transform(heat_src);
        heat_src_fft_done = true;
    }
 
    // Assign temperatures and heat rates to the respective matrices
    for (auto &par : system)
    {
        int i = std::round((par.getPosition()[0] - this->range[0]) / dx);
        int j = std::round((par.getPosition()[1] - this->range[0]) / dx);
        current_temp(i, j) = static_cast<MaterialPoint &>(par).getTemperature();
        heat_rates(i, j) = static_cast<MaterialPoint &>(par).getHeatRate();
    }
 
    // Fourier Transform of current temperature
    Matrix<std::complex<double>> current_temp_hat = FFT::transform(current_temp);
 
    // Compute spatial frequencies
    Matrix<std::complex<double>> freq = FFT::computeFrequencies(mat_size);
    freq /= 1.0 / M_PI; // Normalize frequencies
 
    // Compute the change in temperature in the frequency domain
    Matrix<std::complex<double>> dtheta_hat_dt(mat_size);
    Real eqConstant = 1.0 / (this->rho * this->heatCapacity);
 
    for (int i = 0; i < mat_size; i++)
    {
        for (int j = 0; j < mat_size; j++)
        {
            // Equation to compute temperature change in the frequency domain
            dtheta_hat_dt(i, j) = eqConstant * (this->heat_src_fft(i, j) - heat_rates(i, j) * current_temp_hat(i, j) * (freq(0, i) * freq(0, i) + freq(0, j) * freq(0, j)));
        }
    }
 
    // Inverse Fourier Transform to get change in temperature in time domain
    Matrix<std::complex<double>> dtheta_dt = FFT::itransform(dtheta_hat_dt);
 
    // Apply change of temperature to each particle
    for (auto &par : system)
    {
        int i = std::round((par.getPosition()[0] - this->range[0]) / dx);
        int j = std::round((par.getPosition()[1] - this->range[0]) / dx);
 
        // Apply boundary condition if needed
        if (this->null_border && (!(i % (mat_size - 1)) || !(j % (mat_size - 1))))
            static_cast<MaterialPoint &>(par).getTemperature() = 0.0;
        else
            // Update the temperature
            static_cast<MaterialPoint &>(par).getTemperature() += this->deltaT * dtheta_dt(i, j).real();
    }
}
/* -------------------------------------------------------------------------- */