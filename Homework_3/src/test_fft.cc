#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

namespace fft_tests {

    const UInt N = 512;
    const Real k = 2 * M_PI / N;

    /**
     * Populates a matrix with cosine values.
     * @param matrix The matrix to populate.
     */
    void populateCosine(Matrix<complex>& matrix) {
        for (auto entry : index(matrix)) {
            auto [i, j, val] = entry;
            val = cos(k * i); // Assign cosine values to matrix elements
        }
    }

    /**
     * Asserts the correctness of FFT transform results.
     * @param matrix The matrix to check.
     */
    void assertTransform(Matrix<complex>& matrix) {
        for (auto entry : index(matrix)) {
            auto [i, j, val] = entry;

            // Print significant values for debugging
            if (std::abs(val) > 1e-10)
                std::cout << i << "," << j << " = " << val << std::endl;

            // Check specific conditions of FFT transform
            if ((i == 1 || i == N - 1) && j == 0)
                ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
            else
                ASSERT_NEAR(std::abs(val), 0, 1e-10);
        }
    }

    // Test for forward FFT transform
    TEST(FFT, transform) {
        Matrix<complex> m(N);
        populateCosine(m);
        Matrix<complex> transformed = FFT::transform(m);
        assertTransform(transformed);
    }

    // Test for inverse FFT transform
    TEST(FFT, inverse_transform) {
        Matrix<complex> m(N);
        populateCosine(m);
        Matrix<complex> transformed = FFT::transform(m);
        Matrix<complex> inverse_transformed = FFT::itransform(transformed);

        for (auto entry : index(inverse_transformed)) {
            auto [i, j, val] = entry;
            auto& orig_val = m(i, j);

            // Check if inverse FFT recovers the original values
            ASSERT_NEAR(std::real(val), std::real(orig_val), 1e-10);
            ASSERT_NEAR(std::imag(val), std::imag(orig_val), 1e-10);
        }
    }

 // Testing the Fast Fourier Transform (FFT) for computing frequencies
TEST(FFT, compute_frequencies) {
    // First test case with N = 8
    UInt N = 8;
    // Computing frequencies using FFT for N = 8
    auto freqs = FFT::computeFrequencies(N);

    // Iterating through each frequency component
    for (auto&& entry : index(freqs)) {
        // Extracting the indices and value from the entry
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);

        // Calculating the expected real and imaginary parts of the frequency
        UInt freq_i = i < N / 2 ? i : i - N;
        UInt freq_j = j < N / 2 ? j : j - N;
        // Asserting that the calculated values match the expected values
        ASSERT_EQ(val.real(), freq_i);
        ASSERT_EQ(val.imag(), freq_j);
    }

    // Second test case with N = 9
    N = 9;
    // Computing frequencies using FFT for N = 9
    freqs = FFT::computeFrequencies(N);

    // Iterating through each frequency component for N = 9
    for (auto&& entry : index(freqs)) {
        // Extracting the indices and value from the entry
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);

        // Calculating the expected real and imaginary parts of the frequency
        // Adjusting the calculation for odd N values
        UInt freq_i = i <= (N - 1) / 2 ? i : i - N;
        UInt freq_j = j <= (N - 1) / 2 ? j : j - N;
        // Asserting that the calculated values match the expected values
        ASSERT_EQ(val.real(), freq_i);
        ASSERT_EQ(val.imag(), freq_j);
    }
}


}


