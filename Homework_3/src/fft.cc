#include "matrix.hh"
#include "my_types.hh"
#include "fft.hh"
#include <complex>
#include <fftw3.h>
#include <iostream>

/**
 * @brief Perform a Fast Fourier Transform on the input matrix.
 * 
 * @param m_in Input matrix of complex numbers.
 * @return Matrix<complex> The transformed matrix.
 */
Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
    UInt size = m_in.size(); // Assuming a square matrix

    // Creating a result matrix of the same size as input
    Matrix<complex> res(size);

    // Planning and executing the FFT
    fftw_plan p = fftw_plan_dft_2d(size, size,
                                   reinterpret_cast<fftw_complex*>(m_in.data()),
                                   reinterpret_cast<fftw_complex*>(res.data()),
                                   FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p); // Clean up the plan

    return res;
}

/**
 * @brief Perform an Inverse Fast Fourier Transform on the input matrix.
 * 
 * @param m_in Input matrix of complex numbers.
 * @return Matrix<complex> The inverse transformed matrix.
 */
Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    UInt size = m_in.size(); // Assuming a square matrix

    // Creating a result matrix of the same size as input
    Matrix<complex> res(size);

    // Planning and executing the inverse FFT
    fftw_plan p = fftw_plan_dft_2d(size, size,
                                   reinterpret_cast<fftw_complex*>(m_in.data()),
                                   reinterpret_cast<fftw_complex*>(res.data()),
                                   FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p); // Clean up the plan

    // Normalization
    res /= (size * size);

    return res;
}

/**
 * @brief Compute frequencies for the FFT operation.
 * 
 * @param size The size of the frequency matrix to be created.
 * @return Matrix<std::complex<double>> The frequency matrix.
 */
Matrix<std::complex<double>> FFT::computeFrequencies(int size) {
    Matrix<std::complex<double>> m_out(size);

    for (auto&& [i, j, value] : index(m_out)) {
        // Calculation logic remains the same
        UInt freq_i = (i < size / 2 || (size % 2 != 0 && i <= (size - 1) / 2)) ? i : i - size/2;
        UInt freq_j = (j < size / 2 || (size % 2 != 0 && j <= (size - 1) / 2)) ? j : j - size/2;

        value.real(freq_i);
        value.imag(freq_j);
    }

    return m_out;
}


