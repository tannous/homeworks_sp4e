#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__
 
#include <complex>
#include <vector>
#include "compute.hh"      // Base Compute class
#include "matrix.hh"       // Matrix class for handling complex matrices
#include "system.hh"       // System class, assumed necessary for the type
 
/*!
 * @brief Class to compute temperature distribution within a given system.
 * Inherits from the Compute class.
 */
class ComputeTemperature : public Compute {
private:
    Real deltaT;                // Time step for temperature computation
    Real rho;                   // Mass density of the material
    Real heatCapacity;          // Specific heat capacity of the material
    Real heatConductivity;      // Thermal conductivity of the material
    bool null_border;           // Flag for applying null temperature at borders
    bool heat_src_fft_done;     // Flag to check if FFT of heat source is computed
 
    std::vector<Real> range;    // Range of particle positions
    Real dx;                    // Spatial step for particle position indexing
    Matrix<std::complex<double>> heat_src_fft; // Fourier transform of heat source
 
public:
    // Constructor
    ComputeTemperature();
 
    // Override compute method from Compute base class
    void compute(System& system) override;
 
    // Setters for physical properties
    void setDeltaT(Real dt);
    void setMassDensity(Real rho);
    void setSpecificHeatCapacity(Real spe_heat_capacity);
    void setHeatConductivity(Real heat_conductivity);
    void setNullBorder(bool null_border);
 
    // Initialize spatial range from particle positions in the system
    void initRangeFromParticles(System& system);
 
    // Additional methods can be added here if needed
};
 
#endif // __COMPUTE_TEMPERATURE__HH__
 
