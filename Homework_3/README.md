# Particle System Simulation Framework Documentation
NOTE: after refactoring the code, a stability issue has appeared, leading to abnormal temperatures. Particles next to each-other can have impossibly different temperatures. Sadly, thursday 14 at 00:02 (3 minutes after the deadline), the issue has not been fixed. We will keep on searching but the solution will probably cone too late.
We appologise for the inconvenience.
(But the test DO pass)
Ahmad, Joseph, Damien
## Overview

This comprehensive README outlines the Particle System Simulation Framework, focusing on the roles and structures of key classes: `MaterialPoint`, `Matrix`, `MaterialPointsFactory`, and `FFT`. Each class plays a vital role in simulating particles with distinct physical and thermal properties, ensuring robust and efficient management of the simulation environment.

## Class Descriptions

 

### MaterialPoint (`material_point.hh`, `material_point.cc`)

 

- **Inheritance and Properties**: Inherits from `Particle` class, encapsulating basic properties like position and velocity. Unique thermal properties include `temperature` and `heat_rate`.

- **Functionality**:

  - **Initialization and State Management**: Uses `initself(std::istream& sstr)` for initialization from input streams and `printself(std::ostream& stream)` to output the particle's state.

  - **Access and Modification**: Methods like `getTemperature()` and `getHeatRate()` for accessing and modifying temperature and heat rate.



### Matrix (`matrix.hh`)

 

- **Design and Core Functionalities**: A template-based class handling matrices of various data types. Supports resizing, element access (`operator()(UInt i, UInt j)`), and provides matrix dimensions (`rows()`, `cols()`, `size()`).

- **Iterator**: Includes `MatrixIterator` for iterating over matrix elements.

- **Mathematical Operations**: Essential for transformations, rotations, and linear algebra computations.

 

### MaterialPointsFactory (`material_points_factory.hh`, `material_points_factory.cc`)

 

- **Inheritance and Design Pattern**: Inherits from `ParticlesFactoryInterface`. Utilizes a factory pattern, possibly following a Singleton design, for controlled creation of `MaterialPoint` objects.

- **Functionality**: Generates a complete simulation system from configuration files, creating individual `MaterialPoint` instances.

 ### FFT (`fft.hh`)

- **Fourier Transform and Dependencies**: Wraps the FFTW library for forward (`transform`) and inverse (`itransform`) Fourier transforms. Relies on `matrix.hh` and `fftw3.h`.
- **Key Functions**: `computeFrequencies(int size)` for frequency analysis in signal processing.

## System Organization and Interaction

- **Particle Representation and Management**: `MaterialPoint` instances represent individual particles, while `Matrix` is utilized for collective attribute manipulation.
- **Systematic Creation and Configuration**: `MaterialPointsFactory` ensures systematic generation and arrangement of `MaterialPoint` objects, aligning with the simulation's requirements.
- **Advanced Mathematical Processing**: `FFT` is integral for computations requiring Fourier analysis, influencing particle behavior and simulation dynamics.


## Conclusion

 

The Particle System Simulation Framework is a sophisticated environment, combining the functionalities of `MaterialPoint`, `Matrix`, `MaterialPointsFactory`, and `FFT`. This synergy allows for detailed simulation of particles, encompassing both physical attributes and advanced mathematical computations, resulting in a dynamic and flexible simulation toolkit.

 

## File References

 

- `MaterialPoint`: `material_point.hh`, `material_point.cc`

- `Matrix`: `matrix.hh`

- `MaterialPointsFactory`: `material_points_factory.hh`, `material_points_factory.cc`

- `FFT`: `fft.hh`

---

# Particles Simulation Program

 
This part of README will cover the following sections:

1. **Introduction**: A brief overview of the program and its purpose.
2. **Requirements**: Necessary libraries and tools needed to run the program.
3. **Installation and Setup**: Steps to set up the project environment.
4. **Building the Code**: Instructions on how to compile and build the program.
5. **Running the Program**: How to execute the program and use its features.
6. **Testing**: How to run the provided tests to ensure the program is working correctly.
7. **Documentation**: Instructions on how to generate and access the documentation.
8. **File Structure**: An overview of the key files and their roles in the project.




## Introduction

The Particles Simulation Program is a computational physics software designed for simulating the dynamics of particle systems. It includes various modules for handling different physical phenomena like gravity, kinetic energy, and temperature.

## Requirements

- C++ Compiler supporting C++14 standard.
- CMake (version 3.1 or higher).
- FFTW3 Library for Fourier transforms.
- GoogleTest for unit testing (optional for running tests).

## Installation and Setup

1. Clone the repository or download the source code.
2. Navigate to the project directory.
3. Initialize and update the GoogleTest submodule (if you plan to run tests):

   ```bash
   git submodule init googletest
   git submodule update googletest
   ```

## Building the Code

1. Create a build directory:

   ```bash
   mkdir build
   cd build
   ```

2. Run CMake to configure the project:

   ```bash
   cmake ../ -DUSE_FFTW=ON
   ```

3. Compile the code:

   ```bash
   make
   ```

## Running the Program

To run the simulation, use the following command format:

```bash
./particles [nsteps] [dump_freq] [input.csv] [particle_type] [timestep]
```

- `nsteps`: Number of simulation steps.
- `dump_freq`: Frequency of data dumping.
- `input.csv`: CSV file with initial particle configurations.
- `particle_type`: Type of particles (e.g., planet, ping_pong, material_point).
- `timestep`: Time step for the simulation.

Example:

```bash
./particles 1000 100 input.csv planet 0.01
```

## Testing

To run the unit tests:

```bash
./test_fft
./test_kepler
```

## Documentation

Generate the Doxygen documentation by running:

```bash
make doc
```

The documentation is available in `build/html/index.html`.


## Generating Heat Distribution Using Python Script

The framework includes a Python script, `generate_heat_distr.py`, which allows users to create a heat distribution file for simulations. The script's usage is as follows:

```bash
python3 generate_heat_distr.py --xlim -1 1 --ylim -1 1 --pn_x 8 --pn_y 8 --radius 0.5 --filename heat_file_input.csv --plot
```

- `--xlim` and `--ylim`: Define the x and y limits of the grid.
- `--pn_x` and `--pn_y`: Specify the number of particles along the x and y axes.
- `--radius`: Sets the radius for heat distribution around each particle.
- `--filename`: Name of the CSV file to store the generated heat distribution data.
- `--plot`: If included, plots the heat distribution for visual inspection.

This script is essential for setting up initial conditions in simulations involving temperature and heat transfer.

---

## Exercise 4.6: Launching Simulation with Paraview for a 512 × 512 Particle Grid

To launch a stimulation for a 512 × 512 particle grid and visualize the results in Paraview, follow these steps:

1. Open Paraview.
2. Go to `File > Open` and select the `heat_file_input.csv` file.
3. In the `Open Data With` dialog, choose `CSV Reader`.
4. In the `Properties` window:
   - Deselect `Have Headers`.
   - Set `Field Delimiter Characters` to a comma (`,`).
   - Click `Apply`.
5. Navigate to `Filters > Search`, search for `Table to Points`, and press `Enter`.
6. In the `Properties` window:
   - Assign X, Y, and Z columns to Field 0, Field 1, and Field 2, respectively.
   - Select `2D points`.
   - Click `Apply`.
7. Under `Display (GeometryRepresentation)`:
   - Change `Coloring` to Field 10 for temperature visualization or Field 11 for heat visualization.
   - Toggle the color legend visibility as needed.

These steps allow for the visualization of temperature and heat distributions within the particle grid, providing insights into the dynamic interactions within the simulation.

---

## File Structure

- `*.cc` and `*.hh`: Source and header files for various modules.
- `main.cc`: Main program entry point.
- `CMakeLists.txt`: CMake configuration file.
- `Doxyfile`: Doxygen configuration file.
- `generate_input.py`: Script to generate initial particle configurations.

Using `python3 generate_heat_distr.py --xlim -1.0 1.0 --ylim -1.0 1.0 --pn_x 8 --pn_y 8 --radius 0.5 --filename custom_heat_distribution.csv --plot`, different heat distribution implementations are achieved. The first set of images are plotted using Python, while the second set are from Paraview with different `pn_x` and `pn_y` settings.

### Results Visualizations

<table>
  <tr>
    <td>
      <img src="Results/22.png" alt="Python-Generated Heat Distribution Map 8_8" width="100%">
      <p align="center"><em></em></p>
    </td>
    <td>
      <img src="Results/11.png" alt="Python-Generated Heat Distribution Map 512_512" width="100%">
      <p align="center"><em></em></p>
    </td>
  </tr>
  <tr>
    <td>
      <img src="Results/111.png" alt="Paraview Visualization - 8_8" width="80%">
      <p align="center"><em></em></p>
    </td>
    <td>
      <img src="Results/222.png" alt="Paraview Visualization - 512_512" width="80%">
      <p align="center"><em></em></p>
    </td>
  </tr>
</table>


