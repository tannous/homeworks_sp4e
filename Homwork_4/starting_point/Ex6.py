import argparse
import csv
import numpy as np
import matplotlib.pyplot as plt
 
import os
import sys

pypart_dir = os.path.join('starting_point', 'build', 'pypart')
sys.path.append('build')
 
from pypart import ParticlesFactoryInterface, PlanetsFactory
from pypart import CsvWriter
from pypart import ComputeGravity, ComputeVerletIntegration
 
from Ex5 import readPositions, computeError
def generateInput(scale, planet_name, input_filename):
    """Modifies an input file by scaling the velocity of a specified planet."""
    output_directory = 'output'
    os.makedirs(output_directory, exist_ok=True)
 
    base_filename = os.path.basename(input_filename)
    scaled_filename = f'scaled_{scale}_{planet_name}_{base_filename}'
    output_filename = os.path.join(output_directory, scaled_filename)
 
    new_rows = []
    planet_found = False
 
    with open(input_filename, 'r') as file, open(output_filename, 'w', newline='') as outfile:
        csvwriter = csv.writer(outfile, delimiter=' ')
 
        for line in file:
            if line.startswith('#'):
                continue
 
            row = line.split()
            if row and row[-1].lower() == planet_name.lower():
                planet_found = True
                velocity = np.array([float(v) for v in row[3:6]]) * scale
                new_row = row[:3] + list(map(str, velocity)) + row[6:]
                csvwriter.writerow(new_row)
            else:
                csvwriter.writerow(row)
 
    if not planet_found:
        print(f"Planet '{planet_name}' not found in {input_filename}.")
        return None
 
    ## print(f'Scaled values for {planet_name} with scale factor {scale} are saved in {output_filename}')
    return output_filename

def launchParticles(input: str, nb_steps: int, freq: int) -> None:
    timestep = 1
 
    PlanetsFactory.getInstance()
    factory = ParticlesFactoryInterface.getInstance()
 
    G = 6.67384e-11 / (149597870.700 * 1e3)**3 * 5.97219e24 * (60 * 60 * 24)**2
 
    compute_grav = ComputeGravity()
    compute_grav.setG(G)
    compute_verlet = ComputeVerletIntegration(timestep)
    compute_verlet.addInteraction(compute_grav)
 
    evol = factory.createSimulation(input, timestep, lambda self, timestep: self.system_evolution.addCompute(compute_verlet))
 
    CsvWriter("out.csv").write(evol.getSystem())
    evol.setNSteps(nb_steps)
    evol.setDumpFreq(freq)
    evol.evolve()
 
    print('Simulation for planet is saved at out.csv')

def runAndComputeError(scale, planet_name, input_file, nb_steps, freq, plot=False):

    """Runs the simulation with scaled input and computes the error."""

    scaled_input_filename = generateInput(scale, planet_name, input_file)
    launchParticles(scaled_input_filename, nb_steps, freq)
    positions = readPositions(planet_name, 'dumps')
    positions_ref = readPositions(planet_name, 'trajectories')
    error = computeError(positions, positions_ref)
    if plot:
        fig, axs = plt.subplots(1, 3, figsize=(12, 3))
        for i, axis in enumerate(['X', 'Y', 'Z']):
            axs[i].plot(positions[:, i], label='Simulated', lw=1.7, color='firebrick')
            axs[i].plot(positions_ref[:, i], label='Reference', lw=1.7, color='steelblue')
            axs[i].set_ylabel(axis)
            axs[i].set_xlabel('Days [#]')
        axs[2].legend()
        plt.suptitle(f'{planet_name} Position (Error: {round(error,3)}, Scale: {scale})')
        fig.savefig(f'{planet_name}_positions.png')
        plt.tight_layout()
        plots_dir = 'plots_Ex6'
        os.makedirs(plots_dir, exist_ok=True)
        ## plt.show()
        plt.savefig(os.path.join(plots_dir, f'{planet_name}_scale{scale}_plot.png'))
    return error

def main():
    parser = argparse.ArgumentParser(description="Particle Simulation and Trajectory Error Analysis")
    parser.add_argument('--planet_name', type=str, required=True, help='Name of the planet')
    parser.add_argument('--filename', type=str, required=True, help='Input file name')
    parser.add_argument('--nb_steps', type=int, required=True, help='Number of simulation steps')
    parser.add_argument('--freq', type=int, required=True, help='Frequency of output generation')
    parser.add_argument('--scale', type=float, required=True, help='Velocity scale factor')
    parser.add_argument('--plot', action='store_true', help='Plot the trajectories')
    args = parser.parse_args()
 
    error = runAndComputeError(args.scale, args.planet_name, args.filename, args.nb_steps, args.freq, args.plot)
    print(f"Computed Error for {args.planet_name}: {error}")
 
if __name__ == "__main__":
    main()
