#include <pybind11/pybind11.h>
#include "compute_temperature.hh"  // Include the header for ComputeTemperature class

namespace py = pybind11;

PYBIND11_MODULE(compute_module, m) {
    // Creating a Python class for ComputeTemperature with shared_ptr management
    py::class_<ComputeTemperature, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
        .def(py::init<>())  // Default constructor binding

        // Property binding for 'conductivity'
        // The getter lambda accesses the private member 'conductivity'
        // The setter lambda modifies the private member 'conductivity'
        .def_property("conductivity",
                      [](const ComputeTemperature &ct) { return ct.conductivity; },
                      [](ComputeTemperature &ct, const double &value) { ct.conductivity = value; },
                      "Access and modify the conductivity value")

        // Property binding for 'L'
        .def_property("L",
                      [](const ComputeTemperature &ct) { return ct.L; },
                      [](ComputeTemperature &ct, const double &value) { ct.L = value; },
                      "Access and modify the L value")

        // Property binding for 'capacity'
        .def_property("capacity",
                      [](const ComputeTemperature &ct) { return ct.capacity; },
                      [](ComputeTemperature &ct, const double &value) { ct.capacity = value; },
                      "Access and modify the capacity value")

        // Property binding for 'density'
        .def_property("density",
                      [](const ComputeTemperature &ct) { return ct.density; },
                      [](ComputeTemperature &ct, const double &value) { ct.density = value; },
                      "Access and modify the density value")

        // Property binding for 'delta_t'
        .def_property("delta_t",
                      [](const ComputeTemperature &ct) { return ct.delta_t; },
                      [](ComputeTemperature &ct, const double &value) { ct.delta_t = value; },
                      "Access and modify the delta_t value")

        // Additional methods and properties can be added here
        ;
}
