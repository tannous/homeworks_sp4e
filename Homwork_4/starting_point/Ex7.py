import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fmin
import os
import sys

# Import your custom functions here. For example:

from Ex6 import runAndComputeError

scale_factors = []
errors = []
iteration_count = 0  # To keep track of the number of iterations

def objective_function(scale, planet_name, input_file, nb_steps, freq):

    """
    Objective function for optimization. Computes the error for a given velocity scale.
    """

    global iteration_count
    error = runAndComputeError(scale, planet_name, input_file, nb_steps, freq)
    scale_factors.append(scale[0])
    errors.append(error)

    iteration_count += 1
    print(f"Iteration: {iteration_count}, Scale: {scale[0]}, Error: {error}")

    return error

def optimize_velocity(planet_name, input_file, nb_steps, freq, initial_scale):

    """
    Optimizes the velocity scaling factor for a given planet.
    """

    optimized_scale = fmin(func=objective_function, x0=[initial_scale], args=(planet_name, input_file, nb_steps, freq))
    return optimized_scale[0]

 
def plot_error_vs_scale(scale_values, errors, planet_name):
    """
    Plots error versus scaling factor and convergence curve.
    """
    fig, axs = plt.subplots(1, 2, figsize=(15, 6))
 
    # Plot error vs. scale factor
    axs[0].plot(scale_values, errors, marker='o', linestyle='-', color='b')
    axs[0].set_title(f'Error vs. Velocity Scale Factor for {planet_name}')
    axs[0].set_xlabel('Velocity Scale Factor')
    axs[0].set_ylabel('Error')
    axs[0].grid(True)
 
    # Plot convergence curve (error vs. iteration)
    axs[1].plot(range(1, len(errors) + 1), errors, marker='o', linestyle='-', color='r')
    axs[1].set_title(f'Convergence Curve for {planet_name}')
    axs[1].set_xlabel('Iteration')
    axs[1].set_ylabel('Error')
    axs[1].grid(True)

    plt.tight_layout()
    plots_dir = 'plots_Ex7'
    os.makedirs(plots_dir, exist_ok=True)
    plt.savefig(os.path.join(plots_dir, f'{planet_name}_Convergence_plot.png'))

def main():
    parser = argparse.ArgumentParser(description="Optimize Planet's Velocity")
    parser.add_argument('--planet_name', type=str, required=True, help='Name of the planet')
    parser.add_argument('--filename', type=str, required=True, help='Input file name')
    parser.add_argument('--nb_steps', type=int, required=True, help='Number of simulation steps')
    parser.add_argument('--freq', type=int, required=True, help='Frequency of output generation')
    parser.add_argument('--initial_scale', type=float, required=True, help='Initial guess for velocity scale factor')
    args = parser.parse_args()

    # Optimize the velocity
    optimal_scale = optimize_velocity(args.planet_name, args.filename, args.nb_steps, args.freq, args.initial_scale)
    print(f'Optimal Velocity Scale for {args.planet_name}: {optimal_scale}')

    # Plot error vs scale and convergence curve
    plot_error_vs_scale(scale_factors, errors, args.planet_name)

if __name__ == "__main__":
    main()
