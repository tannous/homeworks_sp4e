import numpy as np
import pandas
import os, os.path

def readPositions(planet_name, directory):
    steps = os.listdir(directory)
    data = np.ndarray([len(steps),3])
    
    for step in steps:
        d = pandas.read_csv(os.path.join(directory, step), sep=" ")
        for line in d.values:

            if line[-1] == planet_name:
                index = int(step[5:-4])
                data[int(index)] =[line[1],line[2],line[3]] 
    return data

def computeError(positions,positions_ref):
    added_error = 0

    if len(positions_ref) < len(positions):
        iter = len(positions_ref)
    else :
        iter = len(positions)

    for i in range(iter):
        local_error = (positions[i]-positions_ref[i])
        added_error =+ local_error[0]**2 + local_error[1]**2 + local_error[2]**2

    error = np.sqrt(added_error)
    return error

def generateInput(scale,planet_name,input_filename,output_filename):
    input_start = pandas.read_csv(input_filename, sep=" ")
    print(input_start)
    for i in range(len(input_start.values)):
        if input_start.values[i][-2] == planet_name:
            input_start.iloc[i,3] *= scale
            input_start.iloc[i,4] *= scale
            input_start.iloc[i,5] *= scale


    print(input_start)

    input_start.to_csv(output_filename, sep=" ", index=False)

ref = readPositions("mercury","./build/trajectories")
mydata = readPositions("mercury","./build/dumps")
err = computeError(mydata, ref)

print(err)

generateInput(100,"mercury","./init.csv","./new_init.csv")