#include <pybind11/pybind11.h>
#include <pybind11/stl.h>  // If you need to bind STL containers
#include <memory>          // For std::shared_ptr

// Include the necessary headers for the classes
#include "compute.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"

namespace py = pybind11;

PYBIND11_MODULE(compute_module, m) {
    // Binding for Compute class
    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute")
        .def(py::init<>()) // Constructor
        // Add other methods from Compute here
        // Example: .def("method_name", &Compute::methodName)

    // Binding for ComputeInteraction class
    py::class_<ComputeInteraction, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction")
        .def(py::init<>()) // Constructor
        // Add other methods from ComputeInteraction here

    // Binding for ComputeGravity class
    py::class_<ComputeGravity, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
        .def(py::init<>()) // Constructor
        // Add other methods from ComputeGravity here

    // Binding for ComputeTemperature class
    py::class_<ComputeTemperature, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
        .def(py::init<>()) // Constructor
        // Add other methods from ComputeTemperature here

    // Binding for ComputeVerletIntegration class
    py::class_<ComputeVerletIntegration, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
        .def(py::init<>()) // Constructor
        // Add other methods from ComputeVerletIntegration here
};
