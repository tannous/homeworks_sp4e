#include <pybind11/pybind11.h>

// Include the headers for the factory classes
#include "particles_factory.hh"
#include "material_points_factory.hh"
#include "planets_factory.hh"
#include "ping_pong_balls_factory.hh"

// Replace this with the actual functor header if needed
#include "functor_type.hh"

namespace py = pybind11;

PYBIND11_MODULE(factory_module, m) {
    // Binding for ParticlesFactory
    py::class_<ParticlesFactory>(m, "ParticlesFactory")
        .def(py::init<>()) // Constructor
        .def("createSimulation", 
             py::overload_cast<const FunctorType&>(&ParticlesFactory::createSimulation),
             "Creates a simulation with a specific configuration based on the passed functor.")
        // Add other methods from ParticlesFactory here, if necessary

    // Binding for MaterialPointsFactory
    py::class_<MaterialPointsFactory>(m, "MaterialPointsFactory")
        .def(py::init<>()) // Constructor
        // Add methods from MaterialPointsFactory here

    // Binding for PlanetsFactory
    py::class_<PlanetsFactory>(m, "PlanetsFactory")
        .def(py::init<>()) // Constructor
        // Add methods from PlanetsFactory here

    // Binding for PingPongBallsFactory
    py::class_<PingPongBallsFactory>(m, "PingPongBallsFactory")
        .def(py::init<>()) // Constructor
        // Add methods from PingPongBallsFactory here
};
