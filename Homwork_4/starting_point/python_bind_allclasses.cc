#include <pybind11/pybind11.h>
// Include the necessary headers for the classes
#include "material_points_factory.hh"
#include "particles_factory_interface.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "csv_writer.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"

namespace py = pybind11;

PYBIND11_MODULE(pypart_module, m) {
    // Binding for MaterialPointsFactory
    py::class_<MaterialPointsFactory, std::shared_ptr<MaterialPointsFactory>>(m, "MaterialPointsFactory")
        .def(py::init<>())  // Constructor
        .def_static("getInstance", &MaterialPointsFactory::getInstance,
                    "Returns the singleton instance of MaterialPointsFactory");

    // Binding for ParticlesFactoryInterface
    py::class_<ParticlesFactoryInterface, std::shared_ptr<ParticlesFactoryInterface>>(m, "ParticlesFactoryInterface")
        .def(py::init<>())  // Constructor
        .def_static("getInstance", &ParticlesFactoryInterface::getInstance,
                    "Returns the singleton instance of ParticlesFactoryInterface");

    // Binding for PingPongBallsFactory
    py::class_<PingPongBallsFactory, std::shared_ptr<PingPongBallsFactory>>(m, "PingPongBallsFactory")
        .def(py::init<>())  // Constructor
        .def_static("getInstance", &PingPongBallsFactory::getInstance,
                    "Returns the singleton instance of PingPongBallsFactory");

    // Binding for PlanetsFactory
    py::class_<PlanetsFactory, std::shared_ptr<PlanetsFactory>>(m, "PlanetsFactory")
        .def(py::init<>())  // Constructor
        .def_static("getInstance", &PlanetsFactory::getInstance,
                    "Returns the singleton instance of PlanetsFactory");

    // Binding for CsvWriter
    py::class_<CsvWriter>(m, "CsvWriter")
        .def(py::init<const std::string &>())  // Constructor
        .def("write", &CsvWriter::write, "Writes data to a CSV file");

    // Binding for ComputeGravity
    py::class_<ComputeGravity, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
        .def(py::init<>())  // Constructor
        .def("setG", &ComputeGravity::setG, "Sets the gravitational constant G");

    // Binding for ComputeVerletIntegration
    py::class_<ComputeVerletIntegration, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
        .def(py::init<double>())  // Constructor with timestep argument
        .def("addInteraction", &ComputeVerletIntegration::addInteraction, "Adds an interaction to the compute");
    
    // Additional class bindings can be added here as needed
}
