#include <pybind11/pybind11.h>
#include <pybind11/functional.h>

namespace py = pybind11;

// Headers for all the required components
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "compute.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "system.hh"
#include "system_evolution.hh"

// Define the Python module
PYBIND11_MODULE(pypart, m) {
    m.doc() = "Python bindings for the Particles project";

    // Particles Factory: Python bindings for creating and managing particle simulations
    py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface", py::dynamic_attr())
        .def("createSimulation", [](ParticlesFactoryInterface &self, const std::string &fname, Real timestep) -> SystemEvolution& {
            return self.createSimulation(fname, timestep); }, py::return_value_policy::reference_internal, py::arg("fname"), py::arg("timestep"))
        .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference_internal, py::arg("fname"), py::arg("timestep"), py::arg("func"))
        .def_static("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
        .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution);

    // Ping Pong Ball Factory: Specialized factory for ping pong ball simulations
    py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory", py::dynamic_attr())
        .def_static("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

    // Planets Factory: Factory for creating planetary simulations
    py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory", py::dynamic_attr())
        .def_static("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);

    // Material Points Factory: Factory for simulations involving material points
    py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory", py::dynamic_attr())
        .def_static("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

    // Compute: Base class for computational components
    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute", py::dynamic_attr());

    // Compute Interaction: Base class for interaction computations
    py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction", py::dynamic_attr());

    // Compute Gravity: Computation for gravitational interactions
    py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity", py::dynamic_attr())
        .def(py::init<>())
        .def("setG", &ComputeGravity::setG, py::arg("G"));

    // Compute Temperature: Computation for temperature-related interactions
    py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature", py::dynamic_attr())
        .def(py::init<>())
        .def_property("conductivity", &ComputeTemperature::getConductivity, [](ComputeTemperature &c, Real val) { c.getConductivity() = val; })
        .def_property("L", &ComputeTemperature::getL, [](ComputeTemperature &c, Real val) { c.getL() = val; })
        .def_property("capacity", &ComputeTemperature::getCapacity, [](ComputeTemperature &c, Real val) { c.getCapacity() = val; })
        .def_property("deltat", &ComputeTemperature::getDeltat, [](ComputeTemperature &c, Real val) { c.getDeltat() = val; })
        .def_property("density", &ComputeTemperature::getDensity, [](ComputeTemperature &c, Real val) { c.getDensity() = val; });

    // Compute Verlet Integration: For integrating motion in simulations
    py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration", py::dynamic_attr())
        .def(py::init<Real>())
        .def("addInteraction", &ComputeVerletIntegration::addInteraction);

    // CSV Writer: Utility for writing simulation data to CSV files
    py::class_<CsvWriter>(m, "CsvWriter", py::dynamic_attr())
        .def(py::init<const std::string&>())
        .def("write", &CsvWriter::write, py::arg("system"));

    // System: Represents a physical system in the simulation
    py::class_<System>(m, "System", py::dynamic_attr())
        .def(py::init<>());

    // System Evolution: Manages the evolution or progression of the simulation over time
    py::class_<SystemEvolution>(m, "SystemEvolution", py::dynamic_attr())
        .def("addCompute", &SystemEvolution::addCompute, py::arg("compute"))
        .def("evolve", &SystemEvolution::evolve)
        .def("getSystem", &SystemEvolution::getSystem)
        .def("setNSteps", &SystemEvolution::setNSteps, py::arg("nsteps"))
        .def("setDumpFreq", &SystemEvolution::setDumpFreq, py::arg("freq"));
}
