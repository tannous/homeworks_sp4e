import pandas as pd
import os
from pypart import PlanetsFactory, ComputeGravity, ComputeVerletIntegration, System

# Constants
AU = 149597870.700  # km
earth_mass = 5.97219e24  # kg

# Function to validate data
def validate_data(row):
    required_columns = ['name', 'x_pos', 'y_pos', 'z_pos', 'x_vel', 'y_vel', 'z_vel', 'mass']
    return all(column in row for column in required_columns)

# Read and validate initial data
try:
    init_data = pd.read_csv('init.csv')
    if not all(init_data.apply(validate_data, axis=1)):
        raise ValueError("Invalid data in init.csv")
except Exception as e:
    print(f"Error reading init.csv: {e}")
    exit(1)

# Initialize the simulation environment
factory = PlanetsFactory.getInstance()
system = System()
compute_grav = ComputeGravity()
compute_verlet = ComputeVerletIntegration(1.0)  # 1 day timestep

# Create planets and set initial state
for index, row in init_data.iterrows():
    planet = factory.createParticle(row['name'])
    planet.setPosition(row['x_pos'] * AU, row['y_pos'] * AU, row['z_pos'] * AU)
    # Correct Mercury's velocity if necessary
    velocity_correction = 1.0 if row['name'] != 'Mercury' else 1.1  # Example correction
    planet.setVelocity(row['x_vel'] * AU * velocity_correction, row['y_vel'] * AU * velocity_correction, row['z_vel'] * AU * velocity_correction)
    planet.setMass(row['mass'] * earth_mass)
    system.addParticle(planet)

# Set up the simulation
compute_grav.setG(6.67430e-11 / AU**3 * earth_mass * (86400**2))  # Adjusted gravitational constant
compute_verlet.addInteraction(compute_grav)
system_evolution.addCompute(compute_verlet)

# Create directory for saving simulation states
os.makedirs('simulated_trajectories', exist_ok=True)

# Function to compare trajectories
def compare_trajectories(simulated_df, expected_df):
    # Implement your comparison logic here
    # For example, calculate the Euclidean distance between positions
    # and sum them up for all planets
    total_distance = 0
    for planet in simulated_df['Name']:
        sim_position = simulated_df[simulated_df['Name'] == planet][['Position']].values[0]
        exp_position = expected_df[expected_df['Name'] == planet][['Position']].values[0]
        distance = np.linalg.norm(sim_position - exp_position)
        total_distance += distance
    return total_distance

# Run the simulation
for day in range(365):
    compute_verlet.integrate(system)

    # Save current state
    current_state = []  # Collect current state data
    for particle in system:
        current_state.append([particle.getName(), particle.getPosition(), particle.getVelocity()])
    current_df = pd.DataFrame(current_state, columns=['Name', 'Position', 'Velocity'])
    current_df.to_csv(f'simulated_trajectories/step-{str(day).zfill(4)}.csv', index=False)

    # Read stored trajectory for comparison
    expected_trajectory = pd.read_csv(f'trajectories/step-{str(day).zfill(4)}.csv')
    comparison_result = compare_trajectories(current_df, expected_trajectory)
    print(f"Day {day}: Total Distance Difference = {comparison_result}")

    

# Post-simulation analysis and comparison
# Add any code here for further analysis or visualization
