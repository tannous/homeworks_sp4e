import argparse
import csv
from pathlib import Path
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def readPosition(planet_name, csv_file_path):
    """
    Reads the position of a planet from a single CSV file.   

    Parameters:
    planet_name (str): Name of the planet to find.
    csv_file_path (str): Path to the CSV file.

    Returns:
    np.ndarray: Numpy array of the planet's position, or an empty array if not found.

    """

    with open(csv_file_path, 'r') as file:
        csv_reader = csv.reader(file, delimiter=' ')
        for row in csv_reader:
            if planet_name.lower() in row:
                return np.array([x for x in row if x != ''][:3], dtype=float)
    return np.array([])

def readPositions(planet_name, directory):
    """
    Reads the position of the planet from all CSV files in a directory
   
    Parameters:
    planet_name (str): Name of the planet to find.
    directory (str): Path to the directory containing CSV files.

    Returns:
    np.ndarray: Numpy array of the planet's trajectory.

    """

    csv_file_names = sorted(Path(directory).rglob('step-*.csv'))
    if not csv_file_names:
        print(f"No CSV files found in directory: {directory}")
        sys.exit(1)
    planet_trajectory = [readPosition(planet_name, file) for file in csv_file_names]
    planet_trajectory = [pos for pos in planet_trajectory if pos.size > 0]

    if not planet_trajectory:
        print(f"No data found for planet {planet_name} in directory: {directory}")
        sys.exit(1)
    return np.array(planet_trajectory)

def computeError(positions, positions_ref):
    """
    Computes the total squared error between computed and reference positions.
   
    Parameters:
    positions (np.ndarray): Numpy array of the computed planet positions.
    positions_ref (np.ndarray): Numpy array of the reference planet positions.
   
    Returns:
    float: Total squared error.
    """
    if positions.shape[0] == 0 or positions_ref.shape[0] == 0:
        print("Error: One of the position arrays is empty. Check your CSV file data.")
        sys.exit(1)
 
    squared_differences = (positions - positions_ref) ** 2
    total_squared_error = np.sum(squared_differences)
    return np.sqrt(total_squared_error)

def plotTrajectories(planet_name, positions, positions_ref, error):
    """
    Plots the computed and reference trajectories in 2D and a slightly enlarged 3D plot in the same layout.
   
    Parameters:
    planet_name (str): Name of the planet.
    positions (np.ndarray): Numpy array of the computed planet positions.
    positions_ref (np.ndarray): Numpy array of the reference planet positions.
    error (float): Total error for the trajectories.
    """
    # Standard figure size
    plt.figure(figsize=(12, 10))
 
    # Plotting 2D trajectories
    for i, axis in enumerate(['X', 'Y', 'Z']):
        plt.subplot(2, 2, i + 1)
        plt.plot(positions[:, i], label='Computed', lw=1.5, color='red')
        plt.plot(positions_ref[:, i], label='Reference', lw=1.5, color='blue')
        plt.title(f'{axis}-axis Trajectory')
        plt.xlabel('Time (days)')
        plt.ylabel(f'{axis} Position')
        plt.legend()

 # Plotting 3D trajectory
    ax3d = plt.subplot(2, 2, 4, projection='3d')
    ax3d.plot(*positions.T, color='red', label='Computed')
    ax3d.plot(*positions_ref.T, color='blue', label='Reference')
    ax3d.set_xlabel('X Position')
    ax3d.set_ylabel('Y Position')
    ax3d.set_zlabel('Z Position')
    ax3d.legend()
 
    # Enlarge the 3D plot to 120% of its original size
    box = ax3d.get_position()
    ax3d.set_position([box.x0, box.y0, box.width * 0.3, box.height * 0.3])
 
    # Display total error in the layout
    plt.suptitle(f'Trajectory Comparison for {planet_name} - Total Error: {error:.2f}')
    plt.tight_layout()
    plots_dir = 'plots_Ex5'
    os.makedirs(plots_dir, exist_ok=True)
    plt.savefig(os.path.join(plots_dir, f'{planet_name}_plot.png'))


def main():
    """
    Main function to handle argument parsing and execute trajectory analysis.

    """
    parser = argparse.ArgumentParser(description="Planetary Trajectory Error Computation")
    parser.add_argument('--planet_name', type=str, required=True, help='Name of the planet')
    parser.add_argument('--directory', type=str, required=True, help='Directory of computed positions')
    parser.add_argument('--directory_ref', type=str, required=True, help='Directory of reference positions')
    parser.add_argument('--plot', action='store_true', help='Flag to plot the trajectories')
    args = parser.parse_args()
    computed_positions = readPositions(args.planet_name, args.directory)
    reference_positions = readPositions(args.planet_name, args.directory_ref)
    error = computeError(computed_positions, reference_positions)
    print(f'Total error for {args.planet_name}: {error}')

    if args.plot:
        plotTrajectories(args.planet_name, computed_positions, reference_positions, error)
if __name__ == "__main__":
    main()