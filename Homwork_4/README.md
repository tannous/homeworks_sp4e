# Homework Exercises 1 to 7:

This document provides comprehensive instructions and insights for completing and understanding the exercises. Each exercise focuses on specific aspects of Python bindings, simulations, and optimizations with detailed guidelines.

## Table of Contents
1. [General Setup](#general-setup)
2. [Exercise 1: Factory Interface](#exercise-1-factory-interface)
3. [Exercise 2: Compute](#exercise-2-compute)
4. [Exercise 3: Other Classes](#exercise-3-other-classes)
5. [Exercise 4: Particle Trajectory Optimization](#exercise-4-particle-trajectory-optimization)
6. [Exercise 5: Planetary Trajectory Error Analysis](#exercise-5-planetary-trajectory-error-analysis)
7. [Exercise 6: Error Calculation with Velocity Scaling](#exercise-6-error-calculation-with-velocity-scaling)

## General Setup

### Prerequisites
Before initiating any Python code, it is crucial to be in the `cmake-build-debug` directory. If submodule errors are encountered, perform the following git commands in the root directory of the repository:

```bash
git submodule add -f https://github.com/pybind/pybind11.git
git submodule add -f https://github.com/google/googletest.git
git submodule add -f https://gitlab.com/libeigen/eigen.git
```

## Exercise 1: Factory Interface

### Objectives
1. Develop Python bindings for factory interface classes: ParticlesFactory, MaterialPointsFactory, PlanetsFactory, and PingPongBallsFactory.
2. Implement Python binding for the overloaded `createSimulation` function in `ParticlesFactory` using `overload_cast`. This function, accepting a functor, enhances the flexibility of simulation customization.

## Exercise 2: Compute

### Objectives
1. Generate Python bindings for Compute classes: Compute, ComputeInteraction, ComputeGravity, ComputeTemperature, and ComputeVerletIntegration.
2. Ensure correct management of references for Compute object types within Python bindings.
3. Enable access and modification of private members in the `ComputeTemperature` class via Python bindings.

## Exercise 3: Other Classes

### Objective
Establish Python bindings for additional essential classes and their functions as needed.

## Exercise 4: Particle Trajectory Optimization

### Objectives and Process
1. Utilize `init.csv` for simulating planet trajectories over 365 days using a daily timestep.
2. Observe and follow the specific unit conventions for distance, time, and mass.
3. Validate planetary dynamics (excluding Mercury) by examining the trajectory data in the `trajectories` directory.


## Running the Simulation

To simulate the trajectories of planets, start by creating a `build` directory. Inside this directory, run the commands `cmake ..` and `make`. Once these steps are completed, you can start the simulation by executing this command:

```bash
python3 main.py 365 1 ../init.csv planet 1
```


Required Arguments

When executing the simulation, specify the following arguments:

- `--main.py`: name of the Python script being executed.
- `--nb_steps`: Determines the number of steps in the simulation is `365` in this problem.
- `--freq`: Sets the data dump frequency during the simulation which is `1` in this case.
- `--filename`: Names the initial or input file for the simulation which is `../init.csv`.
- `--particle_type`: Specifies the particle type for simulation `planet`
- `--timestep`: Defines the timestep for each simulation step `1`.


- In the first GIF, in the simulation: Mercury, shown in blue, is moving straight away instead of following its orbit. This was found using Paraview after use the init.csv and run python3 main.py 365 1 ../init.csv planet 1
- The second GIF corresponds to data from the 'trajectories' directory, where each file named 'step-XXXX.csv' contains the positional information of all planets.



<table>
  <tr>
    <td><img src="Results/Ex4.gif" alt="GIF 1" width="500"/></td>
    <td><img src="Results/Ex4traj.gif" alt="GIF 2" width="510"/></td>
  </tr>
  <tr>
    <td align="center">run init.csv/Computed </td>
    <td align="center">using trajectories/Reference </td>
  </tr>
  <td colspan="3" style="text-align: center; font-weight: bold; font-size: 16px;"> Ex4 solution using trajectories and init.csv </td>
</table>

## Exercise 5: Planetary Trajectory Error Analysis

To run the code for Exercise 5 and analyze the trajectory of Neptune, you would use the following command in the terminal in the source code:

```bash
python3 Ex5.py --planet_name mercury --directory build/dumps --directory_ref trajectories --plot
```

Here's what this command does:

- `python3 Ex5.py`: This executes the Python script named `Ex5.py`.
- `--planet_name mercury`: This specifies that the planet you are analyzing is mercury.
- `--directory build/dumps`: This is the path to the directory where the computed trajectory data for the simulation is stored.
- `--directory_ref trajectories`: This is the path to the directory containing the reference trajectory data against which the simulation data will be compared.
- `--plot`: This flag indicates that the script should generate plots to visualize the comparison between the computed and reference trajectories.

After running this command, the script will read the trajectory data for the planet, compute the error between the simulated and reference trajectories, and generate plots to visually compare them. The results of the error between the computed and reference trajectories are shown below


<table>
  <tr>
    <td><img src="starting_point/plots_Ex5/mercury_plot.png" alt="Computed Trajectory for mercury" width="500"/></td>
    <td><img src="starting_point/plots_Ex5/earth_plot.png" alt="Reference Trajectory for Earth" width="500"/></td>
  </tr>
  <tr>
    <td align="center">Error calculations of Mercury</td>
    <td align="center">Error calculations of Earth</td>
  </tr>
  <td colspan="3" style="text-align: center; font-weight: bold; font-size: 16px;"> Trajectory Analysis for Ex5 </td>
</table>


## Exercise 6: Error Calculation with Velocity Scaling
To run and analyze the trajectory of a planet using the `Ex6.py` script, you would use a command similar to the one used in `Ex5.py`, but with additional parameters specific to `Ex6.py`. Here's an example command:

```bash
python3 Ex6.py --planet_name mercury --filename init.csv --nb_steps 365 --freq 1 --scale 5 --plot
```

This command does the following:

- `python3 Ex6.py`: Executes the Python script named `Ex6.py`.
- `--planet_name mercury`: Specifies that the planet being analyzed is mercury.
- `--filename init.csv`: Indicates the input file name that contains initial conditions for the simulation.
- `--nb_steps 365`: Sets the number of simulation steps to be performed.
- `--freq 1`: Determines the frequency of output generation during the simulation.
- `--scale 5`: Applies a scaling factor to the velocity of the specified planet. In this case, the velocities will be scaled by 5 times their original value.
- `--plot`: A flag indicating that the script should generate plots to visualize the simulated trajectory compared to a reference trajectory.

When this command is run, `Ex6.py` performs several actions:

1. It scales the velocity of the planet in the input file by the specified factor.
2. It runs a simulation of the planetary system using the modified input file.
3. It computes the error between the simulated trajectory and a reference trajectory.
4. If the `--plot` flag is included, it generates and saves plots showing both trajectories and the computed error.

The figures below illustrate the cumulative error associated with various scaled velocities for different planets.

<table>
  <tr>
    <td align="center"><img src="starting_point/plots_Ex6/mercury_scale5.0_plot.png" alt="Computed Trajectory for Mercury scale 5" width="900"/></td>
  </tr>
  <tr>
    <td align="center">Error calculations of Mercury scale 5</td>
  </tr>
  <tr>
    <td align="center"><img src="starting_point/plots_Ex6/mercury_scale0.5_plot.png" alt="Reference Trajectory for Mercury scale 0.5" width="900"/></td>
  </tr>
  <tr>
    <td align="center">Error calculations of Mercury scale 0.5 </td>
  </tr>
  <tr>
    <td colspan="1" style="text-align: center; font-weight: bold; font-size: 16px;">Trajectory Analysis for Ex6</td>
  </tr>
</table>


## Exercise 7: Optimizing Velocity Scaling with Error Minimization

To optimize the velocity scaling factor of a planet's trajectory using the `Ex7.py` script, you execute the script with parameters tailored to the needs of this exercise. Here is an example command for running `Ex7.py`:

```bash
python3 Ex7.py --planet_name mercury --filename init.csv --nb_steps 365 --freq 1 --initial_scale 1.0
```

This command performs the following actions:

- `python3 Ex7.py`: Runs the Python script `Ex7.py`.
- `--planet_name mercury`: Specifies that the planet being optimized is Mercury.
- `--filename init.csv`: Indicates the input file name containing the initial conditions for the simulation.
- `--nb_steps 365`: Sets the number of steps for the simulation.
- `--freq 1`: Sets the frequency of output generation during the simulation.
- `--initial_scale 1.0`: Provides the initial guess for the velocity scaling factor.

When this command is executed, `Ex7.py` carries out several operations:

1. **Optimization of Velocity Scale**:
   - The script uses an optimization algorithm to find the velocity scale factor that minimizes the error in the planet's simulated trajectory.
   - The initial guess for the scale factor is provided via the `--initial_scale` parameter.

2. **Error Calculation**:
   - For each scale factor tested during optimization, the script calculates the error between the simulated trajectory and a reference trajectory.

3. **Plotting Results**:
   - Upon completion of the optimization process, the script generates plots showing:
     - The error versus the velocity scale factor.
     - The convergence curve, illustrating how the error changes with each iteration of the optimization algorithm.

4. **Output**:
   - The optimal velocity scale factor is printed out.
   - The generated plots are saved in a specified directory, helping visualize the optimization process and its results.

<table>
  <tr>
    <td align="center"><img src="Results/Ex7.png" alt="Computed Trajectory for Mercury scale 5" width="900"/></td>
  </tr>
  <tr>
    <td align="center">Results of Ex7 </td>
  </tr>
  <tr>
</td>

</table>




- **Optimization Status**: Successfully Completed
- **Optimal Velocity Scale**: 0.39895
- **Final Error Value**: 0.516509
- **Total Iterations**: 18
- **Function Evaluations**: 37

 The figures generated offer valuable insights into the relationship between velocity scaling and trajectory accuracy, essential for understanding and improving planetary simulation models as shown previously.



The presented results showcase simulations of Mercury's movements under various scenarios, including the Reference case, as well as instances where the scale is adjusted to 0.5, 5, and an optimal condition.
 
<table>
  <tr>
    <td><img src="Results/Reference.gif" alt="Reference Case" width="500"/></td>
    <td><img src="Results/scaled half..gif" alt="Scaled 0.5 Case" width="520"/></td>
  </tr>
  <tr>
    <td align="center">Reference Case</td>
    <td align="center">Scaled 0.5 Case</td>
  </tr>
  <tr>
    <td><img src="Results/scaled 5.gif" alt="Scaled 5 Case" width="500"/></td>
    <td><img src="Results/optimal.gif" alt="Optimal Case" width="500"/></td>
  </tr>
  <tr>
    <td align="center">Scaled 5 Case</td>
    <td align="center">Optimal Case</td>
  </tr>
  <tr>
    <td colspan="2" style="text-align: center; font-weight: bold; font-size: 16px;">Ex4 Mercury Movement Simulations</td>
  </tr>
</table>




---
